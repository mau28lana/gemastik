using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorChanger : MonoBehaviour
{
    public Image image;
    PlayerInventory PI;

    void Start()
    {
        image = GetComponent<Image>();
        var color = image.color;
        color.a = 0.1f;
        image.color = color;
        print(PI.NumberOfItem);
    }

    void Update()
    {
        // if(PI.NumberOfItem != null)
        // {
        //     image = GetComponent<Image>();
        //     var color = image.color;
        //     color.a = 10f;
        //     image.color = color;
        // }
    }
}
