using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartCol : MonoBehaviour
{
    [SerializeField] private float amountExtinguishedPerSecond = 1.0f;
    void OnParticleCollision(GameObject go)
    {
        if(GetComponent<Collider>().TryGetComponent(out Fire fire))
        {
            fire.TryExtinguish(amountExtinguishedPerSecond * Time.deltaTime);
            fire.Play();
        }
        else
        {
            fire.Stop();
        }
    }
}
