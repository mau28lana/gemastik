using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObject : MonoBehaviour
{
    public GameObject gameObject;
    
    public void OpenCanvas()
    {
        if(gameObject != null)
        {
            bool isActive = gameObject.activeSelf;
            gameObject.SetActive(!isActive);
        }
        else if(gameObject == null){
            bool isActive = gameObject.activeSelf;
            gameObject.SetActive(isActive);
        }
    }
}
